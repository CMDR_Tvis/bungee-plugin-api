package io.github.commandertvis.bungee.plugin.command

import io.github.commandertvis.bungee.plugin.command.dsl.CommandScope
import io.github.commandertvis.bungee.plugin.pluginManager
import net.md_5.bungee.api.CommandSender
import net.md_5.bungee.api.plugin.Command

/**
 * Represents a [Command], that uses [CommandScope] as source of its actions and information
 */
public class ManagedCommand @PublishedApi internal constructor(private val scope: CommandScope) :
    Command(scope.aliases.first(), null, *scope.aliases.drop(1).toTypedArray()) {

    init {
        pluginManager.registerCommand(scope.plugin, this)
    }

    /**
     * Executes the command, returning its success
     *
     * @param sender source object which is executing this command
     * @param args all arguments passed to the command, split via ' '
     * @return true
     */
    public override fun execute(
        sender: CommandSender,
        args: Array<out String>
    ): Unit = scope.addResultExceptionHandler(
        runCatching { scope.execution?.execute(sender, args.toMutableList()) },
        sender,
        args.asList()
    )
}
