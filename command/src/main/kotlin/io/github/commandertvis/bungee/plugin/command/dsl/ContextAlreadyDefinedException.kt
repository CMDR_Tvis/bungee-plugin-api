package io.github.commandertvis.bungee.plugin.command.dsl

import kotlin.reflect.KClass

/**
 * Thrown, when [CommandScope.addContext] tries to redefine a registered command context
 *
 * @param clazz the class that was tried to redefine
 */
public class ContextAlreadyDefinedException @PublishedApi internal constructor(clazz: KClass<*>) :
    RuntimeException("class ${clazz.qualifiedName} already has a registered command context")
