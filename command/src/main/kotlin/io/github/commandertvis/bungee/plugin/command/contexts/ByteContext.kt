package io.github.commandertvis.bungee.plugin.command.contexts

/**
 * The [io.github.commandertvis.bungee.plugin.command.contexts.CommandContext] implementation for [Byte] type
 */
public object ByteContext : OneStringContext<Byte>() {
    public override fun resolve(string: String): Byte? = string.toByteOrNull()
}
