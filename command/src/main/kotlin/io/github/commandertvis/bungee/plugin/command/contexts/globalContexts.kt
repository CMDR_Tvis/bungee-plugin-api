package io.github.commandertvis.bungee.plugin.command.contexts

import net.md_5.bungee.api.connection.ProxiedPlayer
import java.math.BigDecimal
import java.math.BigInteger
import java.util.*
import kotlin.reflect.KClass

/**
 * Command contexts of primitive types and a few Spigot API ones
 */
public val globalContexts: Map<KClass<out Any>, CommandContext<out Any>> = mapOf(
    BigDecimal::class to BigDecimalContext,
    BigInteger::class to BigIntegerContext,
    Boolean::class to BooleanContext,
    Byte::class to ByteContext,
    Char::class to CharContext,
    Double::class to DoubleContext,
    Float::class to FloatContext,
    Int::class to IntContext,
    Long::class to LongContext,
    ProxiedPlayer::class to ProxiedPlayerContext,
    Short::class to ShortContext,
    String::class to StringContext,
    UUID::class to UuidContext
)
