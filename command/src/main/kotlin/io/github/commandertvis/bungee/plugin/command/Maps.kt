package io.github.commandertvis.bungee.plugin.command

import io.github.commandertvis.bungee.plugin.command.contexts.CommandContext
import kotlin.reflect.KClass
import kotlin.reflect.KType
import kotlin.reflect.full.createType

internal fun Map<KClass<*>, CommandContext<*>>.mapToTypes(): Map<KType, CommandContext<*>> =
    mutableMapOf<KType, CommandContext<*>>().also { map -> forEach { (t, u) -> map[t.createType()] = u } }
