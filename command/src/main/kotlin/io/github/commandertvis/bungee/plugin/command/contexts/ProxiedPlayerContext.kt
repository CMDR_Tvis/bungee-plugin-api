package io.github.commandertvis.bungee.plugin.command.contexts

import io.github.commandertvis.bungee.plugin.findPlayer
import net.md_5.bungee.api.connection.ProxiedPlayer

/**
 * The [io.github.commandertvis.bungee.plugin.command.contexts.CommandContext] implementation for [ProxiedPlayer] type
 */
object ProxiedPlayerContext : OneStringContext<ProxiedPlayer>() {
    override fun resolve(string: String): ProxiedPlayer? = findPlayer(string)
}
