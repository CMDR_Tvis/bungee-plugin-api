package io.github.commandertvis.bungee.plugin

import io.github.commandertvis.bungee.plugin.command.dsl.CommandScope
import net.md_5.bungee.api.plugin.Plugin

/**
 * Builds a new command, applies data there and registers
 *
 * For example:
 *
 * ```
 * command("plugin", "command") {
 *      description = "The useful command"
 *      execution { default { _, _ -> sendMessage("You've accessed the useful command") } }
 * }
 * ```
 *
 * @param name the command's name - single alias
 * @return new [CommandScope] reference
 */
public inline fun Plugin.command(name: String, block: CommandScope.() -> Unit): CommandScope =
    CommandScope(name, this).apply {
        block()
        injectToBukkit()
    }

/**
 * Builds a new command, applies data there and registers
 *
 * For example:
 *
 * ```
 * command("plugin", setOf("command", "cmd")) {
 *      description = "The useful command"
 *      execution { default { _, _ -> sendMessage("You've accessed the useful command") } }
 * }
 * ```
 *
 * @param aliases the command's aliases
 * @return new [CommandScope] reference
 */
public inline fun Plugin.command(aliases: Set<String>, block: CommandScope.() -> Unit): CommandScope =
    CommandScope(aliases, this).apply {
        block()
        injectToBukkit()
    }
