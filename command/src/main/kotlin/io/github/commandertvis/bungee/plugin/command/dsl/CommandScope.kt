package io.github.commandertvis.bungee.plugin.command.dsl

import io.github.commandertvis.bungee.plugin.command.ManagedCommand
import io.github.commandertvis.bungee.plugin.command.contexts.CommandContext
import io.github.commandertvis.bungee.plugin.command.contexts.globalContexts
import net.md_5.bungee.api.CommandSender
import net.md_5.bungee.api.plugin.Plugin
import kotlin.reflect.KClass

/**
 * Represents a builder scope of a game command.
 *
 * @throws IllegalArgumentException when the set of aliases is empty.
 */
@CommandDslMarker
public class CommandScope @PublishedApi @Throws(IllegalArgumentException::class) internal constructor(
    /**
     * This command's calling aliases.
     */
    public val aliases: Set<String>,
    /**
     * The plugin, owning this command.
     */
    public val plugin: Plugin
) {
    @PublishedApi
    internal constructor(singleAlias: String, plugin: Plugin) : this(setOf(singleAlias), plugin)

    @PublishedApi
    internal val contexts: MutableMap<KClass<out Any>, CommandContext<out Any>> = globalContexts.toMutableMap()

    /**
     * The action, executed, when exception occurs during command execution or tab completing.
     */
    public var exceptionHandler: ((sender: CommandSender, args: List<String>, throwable: Throwable) -> Unit)? = null

    /**
     * The execution section of this [CommandScope].
     */
    public var execution: ExecutionScope? = null
        private set

    init {
        if (aliases.isEmpty())
            throw IllegalArgumentException("command needs at least one alias")
    }

    /**
     * Registers a new command context to have it available in this command.
     *
     * @param T the type, that the new context serializes.
     * @param classOfT the class reference of [T].
     * @param context the new command context.
     * @throws ContextAlreadyDefinedException when there's already registered context of [T].
     * @see ExecutionScope.parseArguments
     */
    @Throws(ContextAlreadyDefinedException::class)
    public fun <T : Any> addContext(classOfT: KClass<T>, context: CommandContext<T>) {
        if (classOfT in contexts)
            throw ContextAlreadyDefinedException(classOfT)

        contexts[classOfT] = context
    }

    /**
     * Registers a new command context to have it available in this command.
     *
     * @param T the type, that the new context serializes.
     * @param context the new command context.
     * @throws ContextAlreadyDefinedException when there's already registered context of [T].
     * @see ExecutionScope.parseArguments
     */
    @Throws(ContextAlreadyDefinedException::class)
    public inline fun <reified T : Any> addContext(context: CommandContext<T>): Unit = addContext(T::class, context)

    /**
     * Sets new [ExecutionScope] in [execution] and applies data there.
     */
    public fun execution(block: ExecutionScope.() -> Unit): Unit = ExecutionScope().let {
        it._command = this
        it.block()
        execution = it
    }

    /**
     * Sets the action, executed, when exception occurs during command execution or tab completing.
     *
     * @param block the new action.
     */
    public fun exceptionHandler(
        block: (sender: CommandSender, args: List<String>, throwable: Throwable) -> Unit
    ): Unit = run { exceptionHandler = block }

    /**
     * Creates a [ManagedCommand] by this scope data and registers it.
     *
     * @return [ManagedCommand], constructed by this scope.
     */
    @PublishedApi
    internal fun injectToBukkit(): ManagedCommand = ManagedCommand(this)

    internal fun addResultExceptionHandler(
        result: Result<*>,
        sender: CommandSender,
        args: List<String>
    ) {
        result.onFailure { exceptionHandler?.invoke(sender, args, it) }
    }
}
