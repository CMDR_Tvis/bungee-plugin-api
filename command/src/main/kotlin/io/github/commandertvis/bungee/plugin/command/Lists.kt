package io.github.commandertvis.bungee.plugin.command

import io.github.commandertvis.bungee.plugin.command.contexts.CommandContext

/**
 * Parses this [List] of [String] to [List] of objects, deserialized iteratively by given command contexts
 *
 * @param contexts the contexts, that deserialize strings
 * @return list of data, parsed of strings by contexts
 */
public fun List<String>.parseArguments(vararg contexts: CommandContext<out Any>): List<Any?> {
    var parsed = 0
    val data = mutableListOf<Any?>()

    contexts.forEach {
        val newParsed = parsed + it.length
        data.add(it.resolve(subList(parsed, newParsed).toTypedArray()))
        parsed = newParsed
    }

    return data
}
