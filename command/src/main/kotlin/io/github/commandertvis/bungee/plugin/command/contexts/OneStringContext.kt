package io.github.commandertvis.bungee.plugin.command.contexts

/**
 * The partial implementation of [CommandContext] that allows to parse data only from single [String]
 *
 * @param T the type, that this [OneStringContext] deserializes
 */
public abstract class OneStringContext<T : Any> : CommandContext<T> {
    public final override val length: Int
        get() = 1

    /**
     * Tries to receive a [T] instance of a given string
     *
     * @param string the input data
     * @return either new [T] instance or null
     */
    public abstract fun resolve(string: String): T?

    public final override fun resolve(strings: Array<String>): T? {
        return resolve(strings.getOrNull(0) ?: return null)
    }
}
