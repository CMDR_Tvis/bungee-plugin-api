package io.github.commandertvis.bungee.plugin.command.contexts

import io.github.commandertvis.bungee.plugin.toUuidOrNull
import java.util.*

/**
 * The [io.github.commandertvis.bungee.plugin.command.contexts.CommandContext] implementation for [UUID] type
 */
public object UuidContext : OneStringContext<UUID>() {
    /**
     * Constructs a [UUID] of the [string].
     *
     * @param string the [String] to be parsed. Example: ```771cdd2e-e524-4218-b0da-21d0fd9a5307```
     */
    public override fun resolve(string: String): UUID? = string.toUuidOrNull()
}
