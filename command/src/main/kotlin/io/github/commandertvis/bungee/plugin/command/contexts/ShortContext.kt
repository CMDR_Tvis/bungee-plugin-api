package io.github.commandertvis.bungee.plugin.command.contexts

/**
 * The [io.github.commandertvis.bungee.plugin.command.contexts.CommandContext] implementation for [Short] type.
 */
public object ShortContext : OneStringContext<Short>() {
    public override fun resolve(string: String): Short? = string.toShortOrNull()
}
