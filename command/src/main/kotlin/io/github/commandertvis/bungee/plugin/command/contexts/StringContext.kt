package io.github.commandertvis.bungee.plugin.command.contexts

/**
 * The [io.github.commandertvis.bungee.plugin.command.contexts.CommandContext] implementation for [String] type.
 */
public object StringContext : OneStringContext<String>() {
    public override fun resolve(string: String): String = string
}
