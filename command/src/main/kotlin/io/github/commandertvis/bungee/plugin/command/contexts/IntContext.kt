package io.github.commandertvis.bungee.plugin.command.contexts

/**
 * The [io.github.commandertvis.bungee.plugin.command.contexts.CommandContext] implementation for [Int] type.
 */
public object IntContext : OneStringContext<Int>() {
    public override fun resolve(string: String): Int? = string.toIntOrNull()
}
