package io.github.commandertvis.bungee.plugin.command.dsl

import io.github.commandertvis.bungee.plugin.command.mapToTypes
import net.md_5.bungee.api.CommandSender
import kotlin.reflect.KClass
import kotlin.reflect.KParameter
import kotlin.reflect.KType
import kotlin.reflect.KVisibility
import kotlin.reflect.full.primaryConstructor

/**
 * Builder scope of command's execution action
 */
@CommandDslMarker
public class ExecutionScope @PublishedApi internal constructor() {
    @PublishedApi
    internal lateinit var _command: CommandScope

    /**
     * The command, where this [ExecutionScope], used
     */
    public inline val command: CommandScope
        get() = _command

    /**
     * The branches that are invoked from this one
     */
    public val children: MutableMap<Set<String>, ExecutionScope> = mutableMapOf()

    /**
     * Already registered command reactions of this branch
     */
    public val actions: MutableMap<Set<String>, CommandSender.(args: List<String>) -> Unit> =
        mutableMapOf()

    /**
     * Default action, invoked in this branch
     */
    public var default: (CommandSender.(args: List<String>) -> Unit)? = null

    /**
     * Adds a new subcommand branch with own features.
     *
     * For example:
     *
     * ```
     * setOf("mySubcommand1", "mySubcommand2") routes { default { _, _, _ -> doWork() } }
     * ```
     *
     * @param block the applied to the new [ExecutionScope] data
     */
    public inline infix fun Set<String>.routes(block: ExecutionScope.() -> Unit): Unit = ExecutionScope().let {
        it.block()
        it._command = command
        children[this] = it
    }

    /**
     * Sets an action, when this group of strings invoked.
     *
     * For example:
     *
     * ```
     * setOf("receiveHelloWorld", "helloWorld", "hW") responds { _, _ -> sendMessage("&a&lHello, world".color()) }
     * ```
     *
     * @param block the action of this subcommand
     */
    public infix fun Set<String>.responds(block: CommandSender.(args: List<String>) -> Unit) {
        actions[this] = block
    }

    /**
     * Sets a default action, when this execution is called
     *
     * @param block the new default action
     */
    public fun default(block: CommandSender.(args: List<String>) -> Unit): Unit = run { default = block }

    /**
     * Sets an action, when this string invoked.
     *
     * For example:
     *
     * ```
     * "receiveHelloWorld" responds { _, _ -> sendMessage("&a&lHello, world".color()) }
     * ```
     *
     * @param block the action of this subcommand
     */
    public infix fun String.responds(block: CommandSender.(args: List<String>) -> Unit): Unit =
        setOf(this) responds block

    /**
     * Adds a new subcommand branch with own features.
     *
     * For example:
     *
     * ```
     * "mySubcommand" routes { default { _, _, _ -> doWork() } }
     * ```
     *
     * @param block the applied to the new [ExecutionScope] data
     */
    public infix fun String.routes(block: ExecutionScope.() -> Unit): Unit = setOf(this) routes block

    /**
     * Parses this list of strings to construct an instance of data class, using registered at this [CommandScope]
     * contexts. If the parsing is impossible, the null pointer is returned. For example:
     *
     * ```
     * data class Foo(val a: Int, val b: String)
     *
     * listOf("123", "abc").parseArguments<Foo>() // Foo(a=123, b=abc)
     *
     * listOf("abc", "123").parseArguments<Foo>() // null
     * ```
     *
     * @param T the class to instantiate, it must be a data class with public constructor
     * @param classOfT the class reference of [T]
     * @return either new instance of [T] or null
     */
    @Throws(IllegalArgumentException::class)
    public fun <T : Any> List<String>.parseArguments(classOfT: KClass<T>): T? {
        if (!classOfT.isData)
            throw IllegalArgumentException("classOfT is supposed to be a data class")

        val constructor = classOfT.primaryConstructor!!

        if (constructor.visibility != KVisibility.PUBLIC)
            throw IllegalArgumentException("the primary constructor of classOfT must be public")

        return runCatching { constructor.call(*constructor.parameters.parseByContexts(this).toTypedArray()) }
            .getOrNull()
    }

    /**
     * Parses this list of strings to construct an instance of data class, using registered at this [CommandScope]
     * contexts. If the parsing is impossible, the null pointer is returned. For example:
     *
     * ```
     * data class Foo(val a: Int, val b: String)
     *
     * listOf("123", "abc").parseArguments<Foo>() // Foo(a=123, b=abc)
     *
     * listOf("abc", "123").parseArguments<Foo>() // null
     * ```
     *
     * @param T the class to instantiate, it must be a data class with public constructor
     * @return either new instance of [T] or null
     */
    @Throws(IllegalArgumentException::class)
    public inline fun <reified T : Any> List<String>.parseArguments(): T? = parseArguments(T::class)

    internal fun execute(sender: CommandSender, args: List<String>) {
        children.forEach {
            args.run {
                if (firstOrNull() in it.key) {
                    it.value.execute(sender, drop(1))
                    return
                }
            }
        }

        actions.forEach {
            args.run {
                if (firstOrNull() in it.key) {
                    it.value(sender, drop(1))
                    return
                }
            }
        }

        default?.invoke(sender, args)
    }

    @Throws(IllegalArgumentException::class)
    private fun List<KParameter>.parseByContexts(strings: List<String>): List<Any?> {
        val data = mutableListOf<Any?>()
        val contextsMapped = command.contexts.mapToTypes()
        var parsed = 0

        asSequence()
            .map(KParameter::type)

            .forEach(fun(typeOfParameter: KType) {
                contextsMapped.forEach { (typeOfContext, context) ->
                    if (typeOfContext.toString().removeSuffix("?") == typeOfParameter.toString().removeSuffix("?")) {
                        val newParsed = parsed + context.length
                        var parsedObject: Any? = null

                        if (newParsed <= strings.size) {
                            val subarray = strings.subList(parsed, newParsed).toTypedArray()
                            parsedObject = context.resolve(subarray)
                        }

                        parsed = newParsed

                        if (parsedObject == null)
                            if (typeOfParameter.isMarkedNullable) {
                                data.add(null)
                                return
                            } else
                                throw IllegalArgumentException(
                                    "impossible to parse a ($parsed, $newParsed) sublist of $strings to $typeOfParameter"
                                )

                        data.add(parsedObject)
                        return
                    }
                }
            })

        if (size > data.size)
            data.addAll(arrayOfNulls(size - data.size))

        return data
    }
}
