package io.github.commandertvis.bungee.plugin.command.dsl

@DslMarker
@Retention(AnnotationRetention.BINARY)
@Target(AnnotationTarget.CLASS)
internal annotation class CommandDslMarker
