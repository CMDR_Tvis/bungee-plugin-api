plugins { id("com.github.johnrengelman.shadow") version "5.1.0" }

base.archivesBaseName = parent!!.name

dependencies {
    implementation(project(":common"))
    implementation(project(":command"))
}

tasks {
    assemble { dependsOn(shadowJar) }

    processResources {
        expand("version" to project.version)
    }
}
