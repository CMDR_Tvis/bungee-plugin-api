package io.github.commandertvis.bungee.plugin

import net.md_5.bungee.api.plugin.Listener
import net.md_5.bungee.api.plugin.Plugin

/**
 * Registers a listener for this plugin.
 *
 * @param listener the listener to register.
 */
public fun Plugin.registerListener(listener: Listener): Unit = pluginManager.registerListener(this, listener)
