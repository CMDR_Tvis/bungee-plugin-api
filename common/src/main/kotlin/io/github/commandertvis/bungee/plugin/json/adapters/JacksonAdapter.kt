package io.github.commandertvis.bungee.plugin.json.adapters

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import kotlin.reflect.KClass

/**
 * Represents a [JsonAdapter] implementation, that uses Jackson [ObjectMapper] for serializing and deserializing
 * objects.
 */
public open class JacksonAdapter public constructor(
    /**
     * [ObjectMapper] instance, used by this adapter to deserialize and serialize objects
     */
    protected val mapper: ObjectMapper
) : JsonAdapter {

    public final override fun <T : Any> fromJson(string: String, classOfT: KClass<out T>): T? =
        runCatching { mapper.readValue(string, classOfT.java) }.getOrNull()

    public override fun toJson(any: Any): String? = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(any)

    public companion object {
        /**
         * The common instance with a simple [ObjectMapper] without additional configuration
         */
        public val COMMON: JacksonAdapter by lazy { JacksonAdapter(jacksonObjectMapper()) }
    }
}
