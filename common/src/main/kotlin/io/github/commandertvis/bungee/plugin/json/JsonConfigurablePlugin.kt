package io.github.commandertvis.bungee.plugin.json

import net.md_5.bungee.api.plugin.Plugin
import java.io.File
import java.nio.charset.Charset

/**
 * Represents a [Plugin] that can use configurations, managed using JSON. All the YAML related behavior is overriden
 * with same actions with JSON configuration.
 *
 * @param C the structure of this configuration
 */
public abstract class JsonConfigurablePlugin<C : Any> public constructor(
    public final override val defaultJsonConfig: C,
    public final override val jsonConfigFileCharset: Charset = Charsets.UTF_8
) : Plugin(), JsonConfiguration<C> {

    private val lazyJsonConfig = LazyJsonConfigurationValue<C, JsonConfigurablePlugin<C>>()

    /**
     * Returns path to `config.json` in [getDataFolder]
     */
    public final override val jsonConfigFilePath: String
        get() = jsonConfigFile.path

    public override val jsonConfigFile: File
        get() = File(
            dataFolder.apply {
                if (!exists())
                    mkdirs()
            },
            "config.json"
        ).apply {
            if (!exists())
                createNewFile()
        }

    /**
     * Contains configuration, parsed with [C] type from file contents, delegated with [lazyJsonConfig]
     */
    public var jsonConfig: C by lazyJsonConfig

    /**
     * Saves the current JSON configuration to the [jsonConfigFile]
     */
    public fun saveConfig(): Unit = saveJsonConfiguration(jsonConfig)

    /**
     * Saves the default JSON configuration to the [jsonConfigFile]
     */
    public fun saveDefaultConfig(): Unit = run { jsonConfig }

    /**
     * Reloads the [jsonConfig] from the contents of [jsonConfigFile]
     */
    public fun reloadConfig(): Unit = lazyJsonConfig.reload(this)
}
