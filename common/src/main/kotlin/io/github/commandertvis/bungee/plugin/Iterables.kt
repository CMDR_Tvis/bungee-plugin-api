package io.github.commandertvis.bungee.plugin

import net.md_5.bungee.api.chat.TextComponent
import net.md_5.bungee.api.connection.ProxiedPlayer

/**
 * Kicks each player of this [Iterable]
 *
 * @param message the kick message
 */
public fun Iterable<ProxiedPlayer>.kickEveryone(message: String): Unit =
    forEach { it.disconnect(TextComponent(message)) }
