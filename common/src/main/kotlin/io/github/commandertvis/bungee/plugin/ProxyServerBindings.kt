package io.github.commandertvis.bungee.plugin

import net.md_5.bungee.api.CommandSender
import net.md_5.bungee.api.ProxyServer
import net.md_5.bungee.api.ReconnectHandler
import net.md_5.bungee.api.config.ConfigurationAdapter
import net.md_5.bungee.api.config.ServerInfo
import net.md_5.bungee.api.connection.ProxiedPlayer
import net.md_5.bungee.api.plugin.PluginManager
import net.md_5.bungee.api.scheduler.TaskScheduler
import java.io.File
import java.net.InetSocketAddress
import java.util.logging.Logger

/**
 * Return all players currently connected.
 *
 * @return all connected players.
 */
public inline val connectedPlayers: List<ProxiedPlayer>
    get() = proxyServer.players.toList()

/**
 * Returns the currently in use configuration adapter.
 *
 * @return the used configuration adapter.
 */
public inline val configurationAdapter: ConfigurationAdapter
    get() = proxyServer.configurationAdapter

/**
 * The [ProxyServer] instance.
 */
public inline val proxyServer: ProxyServer
    get() = ProxyServer.getInstance()

/**
 * Get the [PluginManager] associated with loading plugins and dispatching events. It is recommended that
 * implementations use the provided PluginManager class.
 *
 * @return the plugin manager.
 */
public inline val pluginManager: PluginManager
    get() = proxyServer.pluginManager

/**
 * Return the folder used to load plugins from.
 *
 * @return the folder used to load plugin.
 */
public inline val pluginFolder: File
    get() = proxyServer.pluginsFolder

/**
 * Get the currently in use reconnect handler.
 *
 * @return the in use reconnect handler.
 */
public inline val reconnectHandler: ReconnectHandler
    get() = proxyServer.reconnectHandler

/**
 * Get a list of all registered plugin channels.
 *
 * @return registered plugin channels.
 */
public inline val registeredChannels: List<String>
    get() = proxyServer.channels.toList()

/**
 * Get the scheduler instance for this proxy.
 *
 * @return the in use scheduler.
 */
public inline val scheduler: TaskScheduler
    get() = proxyServer.scheduler

/**
 * Return all servers registered to this proxy, keyed by name. Unlike the methods in [ConfigurationAdapter.getServers],
 * this will not return a fresh map each time.
 *
 * @return all registered remote server destinations.
 */
public inline val connectedServers: Map<String, ServerInfo>
    get() = proxyServer.servers

/**
 * Gets the commands which are disabled and will not be run on this proxy.
 *
 * @return the set of disabled commands.
 */
public inline val proxyServerDisabledCommands: Collection<String>
    get() = proxyServer.disabledCommands

/**
 * Get the current number of connected users. The default implementation is more efficient than [connectedPlayers] as
 * it does not take a lock or make a copy.
 *
 * @return the current number of connected players.
 */
public inline val proxyServerOnlineCount: Int
    get() = proxyServer.onlineCount

/**
 * Gets the main logger which can be used as a suitable replacement for [System.out] and [System.err].
 *
 * @return the [Logger] instance
 */
public inline val proxyServerLogger: Logger
    get() = proxyServer.logger

/**
 * Returns the console overlord for this proxy. Being the console, this command server cannot have permissions or
 * groups, and will be able to execute anything.
 *
 * @return the console command sender of this proxy.
 */
public inline val proxyServerConsole: CommandSender
    get() = proxyServer.console

/**
 * Gets the name of the currently running proxy software.
 *
 * @return the name of this instance
 */
public inline val proxyServerName: String
    get() = proxyServer.name

/**
 * Gets the version of the currently running proxy software.
 *
 * @return the version of this instance
 */
public inline val proxyServerVersion: String
    get() = proxyServer.version

/**
 * Factory method to construct an implementation specific server info instance.
 *
 * @param name name of the server.
 * @param address connectable Minecraft address and port of the server.
 * @param motd the motd when used as a forced server.
 * @param restricted whether the server info restricted property will be set.
 * @return the constructed instance.
 */
public fun constructServerInfo(
    name: String,
    address: InetSocketAddress,
    motd: String,
    restricted: Boolean
): ServerInfo = proxyServer.constructServerInfo(name, address, motd, restricted)

/**
 * Tries to find a [ProxiedPlayer] with certain nickname.
 *
 * @param name the name of player.
 * @return either a player with needed name or null.
 */
public fun findPlayer(name: String): ProxiedPlayer? = connectedPlayers.find { it.name == name }

/**
 * Attempts to match any players with the given name, and returns a list of all possible matches.
 *
 * The exact algorithm to use to match players is implementation specific, but in general you can expect this method to
 * return player's whose names begin with the specified prefix.
 *
 * @param match the partial or complete name to match.
 * @return list of all possible players, singleton if there is an exact match.
 */
public fun matchPlayer(match: String): List<ProxiedPlayer> = proxyServer.matchPlayer(match).toList()

/**
 * Gets the server info of a server.
 *
 * @param name the name of the configured server.
 * @return the server info belonging to the specified server.
 */
public fun serverInfo(name: String): ServerInfo = proxyServer.getServerInfo(name)

/**
 * Register a channel for use with plugin messages. This is required by some server / client implementations.
 *
 * @param channel the channel to register.
 */
public fun registerChannel(channel: String): Unit = proxyServer.registerChannel(channel)

/**
 * Unregister a previously registered channel.
 *
 * @param channel the channel to unregister.
 */
public fun unregisterChannel(channel: String): Unit = proxyServer.unregisterChannel(channel)
