package io.github.commandertvis.bungee.plugin

import io.github.commandertvis.bungee.plugin.chatcomponents.ChatComponentsScope
import net.md_5.bungee.api.ChatMessageType
import net.md_5.bungee.api.CommandSender
import net.md_5.bungee.api.connection.ProxiedPlayer

/**
 * Creates a [ChatComponentsScope], applies an extension function there and broadcasts it. For example:
 *
 * ```
 * broadcastComponents {
 *  text { +"Hello!" }
 *  color(ChatColor.RED)
 *  bold()
 * }
 * ```
 *
 * @param block the extension function to apply
 */
public inline fun broadcastComponents(block: ChatComponentsScope.() -> Unit) = proxyServer
    .broadcast(*ChatComponentsScope().apply(block).components.toTypedArray())

/**
 * Creates a [ChatComponentsScope], applies an extension function. For
 * example:
 *
 * ```
 * player.chatComponents {
 *  text { +"Hello!" }
 *  color(ChatColor.RED)
 *  bold()
 * }
 * ```
 *
 * @param block the extension function to apply
 */
public inline fun CommandSender.replyComponents(block: ChatComponentsScope.() -> Unit): Unit =
    ChatComponentsScope().run {
        block()
        sendMessage(*components.toTypedArray())
    }

/**
 * Creates a [ChatComponentsScope], applies an extension function there and sends it to each receiver of this
 * [Iterable]. For example:
 *
 * ```
 * player.chatComponents {
 *  text { +"Hello!" }
 *  color(ChatColor.RED)
 *  bold()
 * }
 * ```
 *
 * @param block the extension function to apply
 */
public inline fun Iterable<CommandSender>.replyComponents(block: ChatComponentsScope.() -> Unit): Unit =
    ChatComponentsScope().run {
        block()
        forEach { it.sendMessage(*components.toTypedArray()) }
    }

/**
 * Creates a [ChatComponentsScope], applies an extension function there and sends it to each player of this
 * [Iterable]. For example:
 *
 * ```
 * sender.chatComponents {
 *  text { +"Hello!" }
 *  color(ChatColor.RED)
 *  bold()
 * }
 * ```
 *
 * @param type the message type
 * @param block the extension function to apply
 */
public inline fun Iterable<ProxiedPlayer>.replyComponents(
    type: ChatMessageType,
    block: ChatComponentsScope.() -> Unit
): Unit =
    ChatComponentsScope().run { block(); forEach { it.sendMessage(type, *components.toTypedArray()) } }

/**
 * Creates a [ChatComponentsScope], applies an extension function. For example:
 *
 * ```
 * sender.chatComponents {
 *  text { +"Hello!" }
 *  color(ChatColor.RED)
 *  bold()
 * }
 * ```
 *
 * @param type the message type
 * @param block the extension function to apply
 */
public inline fun ProxiedPlayer.replyComponents(type: ChatMessageType, block: ChatComponentsScope.() -> Unit): Unit =
    ChatComponentsScope().run { block(); sendMessage(type, *components.toTypedArray()) }

/**
 * Constructs a [ChatComponentsScope] and applies an extension function there. For example:
 *
 * ```
 * chatComponents {
 *  text { +"Hello!" }
 *  color(ChatColor.RED)
 *  bold()
 * }
 * ```
 *
 * @param block the extension function to apply
 * @return new [ChatComponentsScope] reference
 */
public inline fun chatComponents(block: ChatComponentsScope.() -> Unit): ChatComponentsScope =
    ChatComponentsScope().apply(block)
