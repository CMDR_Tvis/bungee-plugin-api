package io.github.commandertvis.bungee.plugin

import net.md_5.bungee.api.Title
import net.md_5.bungee.api.connection.ProxiedPlayer

/**
 * Constructs a [Title], applies an extension function.
 *
 * @param block the extension function.
 * @return the new [Title] object.
 */
inline fun title(block: Title.() -> Unit): Title = proxyServer.createTitle().apply(block)

/**
 * Constructs a [Title], applies an extension function, sends it to a player.
 *
 * @param block the extension function.
 */
inline fun ProxiedPlayer.sendTitle(block: Title.() -> Unit): Unit = sendTitle(title(block))
