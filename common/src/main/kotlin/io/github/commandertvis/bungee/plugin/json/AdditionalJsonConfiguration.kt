package io.github.commandertvis.bungee.plugin.json

import net.md_5.bungee.api.plugin.Plugin
import java.io.File

/**
 * Represents an additional configuration file, attached to a plugin, for example for data storage management - not
 * settings only
 *
 * @param C the structure of this configuration
 */
public abstract class AdditionalJsonConfiguration<C : Any> public constructor(
    public final override val defaultJsonConfig: C,
    private val plugin: Plugin,
    private val name: String
) : JsonConfiguration<C> {

    private val lazyJsonConfig = LazyJsonConfigurationValue<C, AdditionalJsonConfiguration<C>>()

    public final override val jsonConfigFile
        get() = File(
            plugin.dataFolder.apply {
                if (!exists())
                    mkdirs()
            },

            "$name.json"
        ).apply {
            if (!exists())
                createNewFile()
        }

    public final override val jsonConfigFilePath: String
        get() = jsonConfigFile.path

    /**
     * Contains configuration, parsed with [C] type from file contents
     */
    public var jsonConfig by lazyJsonConfig

    /**
     * Saves the current JSON configuration to the [jsonConfigFile]
     */
    public fun saveConfig(): Unit = saveJsonConfiguration(jsonConfig)

    /**
     * Saves the default JSON configuration to the [jsonConfigFile]
     */
    public fun saveDefaultConfig(): Unit = run { jsonConfig }

    /**
     * Reloads the [jsonConfig] from the contents of [jsonConfigFile]
     */
    public fun reloadConfig(): Unit = lazyJsonConfig.reload(this)
}
