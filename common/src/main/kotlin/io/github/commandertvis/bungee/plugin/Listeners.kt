package io.github.commandertvis.bungee.plugin

import net.md_5.bungee.api.plugin.Listener
import net.md_5.bungee.api.plugin.Plugin

/**
 * Registers this listener with a certain plugin.
 *
 * @param plugin the plugin for registering this listener.
 */
public fun Listener.register(plugin: Plugin): Unit = plugin.registerListener(this)
