package io.github.commandertvis.bungee.plugin.json

import kotlin.properties.ReadWriteProperty
import kotlin.reflect.KProperty

/**
 * Represents a simple lazy-pulled JSON configuration value.
 *
 * @param C the structure of configuration to store with this [LazyJsonConfigurationValue].
 * @param R [JsonConfiguration] object to delegate by this [LazyJsonConfigurationValue] with.
 */
public data class LazyJsonConfigurationValue<C : Any, R : JsonConfiguration<C>> internal constructor(
    @PublishedApi
    internal var value: C? = null
) : ReadWriteProperty<R, C> {

    /**
     * Updates the value, stored in this [LazyJsonConfigurationValue].
     */
    public fun reload(thisRef: R) = run { value = thisRef.getJsonConfiguration() }

    /**
     * Loads the value from [JsonConfiguration.jsonConfigFile] or returns the cached one.
     *
     * @param thisRef [JsonConfiguration], that manages by this value of [C].
     * @param property the delegated property reference.
     * @return the loaded from file value or the cached one.
     */
    public override fun getValue(thisRef: R, property: KProperty<*>): C {
        if (value == null)
            value = thisRef.getJsonConfiguration()

        return value!!
    }

    /**
     * Saves the value to [JsonConfiguration.jsonConfigFile].
     *
     * @param thisRef [JsonConfiguration], that manages this value of [C].
     * @param property the delegated property reference.
     */
    public override fun setValue(thisRef: R, property: KProperty<*>, value: C) {
        this.value = value
        thisRef.saveJsonConfiguration(value)
    }
}
