package io.github.commandertvis.bungee.plugin.json.adapters

import com.google.gson.Gson
import com.google.gson.GsonBuilder
import kotlin.reflect.KClass

/**
 * Represents a [JsonAdapter] implementation that uses [Gson] for serializing and deserializing objects.
 */
public open class GsonAdapter public constructor(
    /**
     * [Gson] instance, used by this adapter to deserialize and serialize objects.
     */
    protected val gson: Gson
) : JsonAdapter {

    public final override fun <T : Any> fromJson(string: String, classOfT: KClass<out T>): T? =
        gson.fromJson<T>(string, classOfT.java)

    public override fun toJson(any: Any): String? = gson.toJson(any)

    public companion object {
        /**
         * The common instance with [Gson], built with [GsonBuilder.setPrettyPrinting] set true
         */
        public val COMMON: GsonAdapter by lazy {
            GsonAdapter(GsonBuilder().run {
                setPrettyPrinting()
                serializeNulls()
                create()
            })
        }
    }
}
