package io.github.commandertvis.bungee.plugin.json

import io.github.commandertvis.bungee.plugin.json.adapters.GsonAdapter
import io.github.commandertvis.bungee.plugin.json.adapters.JsonAdapter
import java.io.File
import java.io.IOException
import java.nio.charset.Charset

/**
 * Represents a typed JSON configuration manager
 *
 * @param C the structure of this configuration
 */
public interface JsonConfiguration<C : Any> {
    /**
     * The adapter instance for JSON conversion. It's implemented as [GsonAdapter.COMMON] reference by default
     */
    @JvmDefault
    public val adapter: JsonAdapter
        get() = GsonAdapter.COMMON

    /**
     * The file, where this configuration stored
     *
     * @throws IOException when an error with file IO occurs
     */
    @JvmDefault
    public val jsonConfigFile: File
        @Throws(IOException::class)
        get() = File(jsonConfigFilePath).apply {
            if (!exists())
                createNewFile()
        }

    /**
     * Charset of attached config file. It is [Charsets.UTF_8] by default.
     */
    @JvmDefault
    public val jsonConfigFileCharset: Charset
        get() = Charsets.UTF_8

    /**
     * The path of [jsonConfigFile]
     */
    public val jsonConfigFilePath: String
    /**
     * Default [C] instance
     */
    public val defaultJsonConfig: C

    /**
     * Tries to retrieve [C] configuration from file, if it is impossible saves [defaultJsonConfig] to the file and
     * returns it
     *
     * @return [C] type configuration, deserialized from file contents, or [defaultJsonConfig]
     */
    @JvmDefault
    public fun getJsonConfiguration(): C {
        adapter.fromJson(jsonConfigFile.readText(jsonConfigFileCharset), defaultJsonConfig::class)?.run {
            return this
        }

        saveJsonConfiguration(defaultJsonConfig)
        return defaultJsonConfig
    }

    /**
     * Overrides the [jsonConfigFile] contents by the needed structure of an object
     *
     * @param value the initial value to write
     * @throws IOException when an error with file IO occurs
     */
    @JvmDefault
    @Throws(IOException::class)
    public fun saveJsonConfiguration(value: C): Unit = jsonConfigFile
        .writeText("${adapter.toJson(value)}\n", jsonConfigFileCharset)
}
