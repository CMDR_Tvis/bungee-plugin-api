package io.github.commandertvis.bungee.plugin.chatcomponents

@DslMarker
@Retention(AnnotationRetention.BINARY)
@Target(AnnotationTarget.CLASS)
internal annotation class ChatComponentsDslMarker
