package io.github.commandertvis.bungee.plugin.json.adapters

import kotlin.reflect.KClass

/**
 * Represents an adapter to serialize and deserialize JSON.
 */
public interface JsonAdapter {
    /**
     * Tries to instantiate [T] by contents of JSON string.
     *
     * @param T the type of object to deserialize.
     * @param string the JSON string.
     * @param classOfT the class reference of [T].
     * @return new [T] instance or null.
     */
    public fun <T : Any> fromJson(string: String, classOfT: KClass<out T>): T?

    /**
     * Tries to create a JSON string by object's contents.
     *
     * @param any the object to serialize.
     */
    public fun toJson(any: Any): String?
}
