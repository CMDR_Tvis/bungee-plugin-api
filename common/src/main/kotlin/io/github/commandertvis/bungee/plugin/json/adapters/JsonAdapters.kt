package io.github.commandertvis.bungee.plugin.json.adapters

/**
 * Tries to instantiate [T] by contents of JSON string.
 *
 * @param T the type of object to deserialize.
 * @param string the JSON string.
 * @return new [T] instance or null.
 */
public inline fun <reified T : Any> JsonAdapter.fromJson(string: String): T? = fromJson(string, T::class)
