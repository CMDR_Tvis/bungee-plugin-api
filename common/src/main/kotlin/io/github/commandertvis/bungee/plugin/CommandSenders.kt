package io.github.commandertvis.bungee.plugin

import net.md_5.bungee.api.CommandSender

/**
 * Dispatches a command on this server, and executes it if found
 *
 * @param commandLine the command with arguments. Example: `test abc 123`
 * @return returns false, if no target is found
 */
public fun CommandSender.command(commandLine: String): Boolean = pluginManager.dispatchCommand(this, commandLine)
