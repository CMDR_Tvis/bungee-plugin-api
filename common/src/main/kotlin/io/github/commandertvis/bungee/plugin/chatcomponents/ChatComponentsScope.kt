package io.github.commandertvis.bungee.plugin.chatcomponents

import net.md_5.bungee.api.ChatColor
import net.md_5.bungee.api.chat.BaseComponent
import net.md_5.bungee.api.chat.ComponentBuilder

/**
 * Structure of chat components to be built with [ComponentBuilder]
 */
@ChatComponentsDslMarker
public class ChatComponentsScope @PublishedApi internal constructor(
    @PublishedApi internal var builder: ComponentBuilder = ComponentBuilder("")
) {
    /**
     * The components of this chatComponents
     */
    public inline val components: List<BaseComponent>
        get() = builder.create().asList()

    /**
     * Changes is the chatComponents bold
     *
     * @param value make this chatComponents bold or not
     */
    public fun bold(value: Boolean = true): Unit = run { builder = builder.bold(value) }

    /**
     * Changes the color
     *
     * @param value color
     */
    public fun color(value: ChatColor = ChatColor.RESET): Unit = run { builder = builder.color(value) }

    /**
     * Changes is the chatComponents italic
     *
     * @param value make this chatComponents italic or not
     */
    public fun italic(value: Boolean = true): Unit = run { builder = builder.italic(value) }

    /**
     * Changes is the chatComponents strikethrough
     *
     * @param value make this chatComponents strikethrough or not
     */
    public fun strikethrough(value: Boolean = true): Unit = run { builder = builder.strikethrough(value) }

    /**
     * Changes is the chatComponents underlined
     *
     * @param value make this chatComponents underlined or not
     */
    public fun underlined(value: Boolean = true): Unit = run { builder = builder.underlined(value) }

    /**
     * Changes is the chatComponents obfuscated
     *
     * @param value make this chatComponents obfuscated or not
     */
    public fun obfuscated(value: Boolean = true): Unit = run { builder = builder.obfuscated(value) }

    /**
     * Appends new [net.md_5.bungee.api.chat.TextComponent] to this chatComponents
     *
     * @param block applied data
     */
    public inline fun text(block: TextContainer.() -> Unit): Unit = component(TextContainer(), block)

    /**
     * Appends new [net.md_5.bungee.api.chat.TranslatableComponent] to this chatComponents
     *
     * @param block applied data
     */
    public inline fun localizedText(block: LocalizedTextContainer.() -> Unit): Unit = component(
        LocalizedTextContainer(), block
    )

    /**
     * Appends new [net.md_5.bungee.api.chat.ScoreComponent] to this chatComponents
     *
     * @param block applied data
     */
    public inline fun score(name: String, objective: String, block: ScoreContainer.() -> Unit): Unit =
        component(ScoreContainer(name, objective), block)

    /**
     * Appends new [net.md_5.bungee.api.chat.KeybindComponent] to this chatComponents
     *
     * @param block applied data
     */
    public inline fun keybind(block: KeybindContainer.() -> Unit): Unit = component(KeybindContainer(), block)

    /**
     * Appends a new chat component to this chatComponents
     *
     * @param block applied data
     */
    public inline fun <T : ChatComponentContainer> component(initial: T, block: T.() -> Unit): Unit = initial.run {
        block()
        this@ChatComponentsScope.let { it.builder = it.builder.append(component) }
    }
}
