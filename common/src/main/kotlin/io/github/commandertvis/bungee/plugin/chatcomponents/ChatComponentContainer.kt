package io.github.commandertvis.bungee.plugin.chatcomponents

import net.md_5.bungee.api.ChatColor
import net.md_5.bungee.api.chat.BaseComponent
import net.md_5.bungee.api.chat.ClickEvent
import net.md_5.bungee.api.chat.HoverEvent

/**
 * This type describes DSL tag of [BaseComponent]
 */
public interface ChatComponentContainer : Appendable {
    /**
     * A component, that is edited by this [ChatComponentContainer]
     */
    public val component: BaseComponent

    /**
     * Changes is the component bold
     *
     * @param value the state if to make this component bold or not
     */
    @JvmDefault
    public fun bold(value: Boolean = true): Unit = run { component.isBold = value }

    /**
     * Changes the color of the component
     *
     * @param value color
     */
    @JvmDefault
    public fun color(value: ChatColor = ChatColor.RESET): Unit = run { component.color = value }

    /**
     * Changes is the component italic
     *
     * @param value the state if to make this component italic or not
     */
    @JvmDefault
    public fun italic(value: Boolean = true): Unit = run { component.isItalic = value }

    /**
     * Changes this component's click action
     *
     * @param action type of action
     * @param value value of [action]
     */
    @JvmDefault
    public fun onClick(action: ClickEvent.Action, value: String) {
        component.clickEvent = ClickEvent(action, value)
    }

    /**
     * Changes this component's hover action
     *
     * @param action type of action
     * @param value value of [action]
     */
    @JvmDefault
    public fun onHover(action: HoverEvent.Action, value: ChatComponentsScope) {
        component.hoverEvent = HoverEvent(action, value.builder.create())
    }

    /**
     * Changes this component's hover action. The value of [action] is specified by a [ChatComponentsScope] that's
     * configured by an extension function
     *
     * @param action type of action
     * @param block the extension function to apply
     */
    @JvmDefault
    public fun onHover(action: HoverEvent.Action, block: ChatComponentsScope.() -> Unit) {
        component.hoverEvent = HoverEvent(action, ChatComponentsScope().apply(block).components.toTypedArray())
    }

    /**
     * Changes is the component strikethrough
     *
     * @param value the state if to make this component strikethrough or not
     */
    @JvmDefault
    public fun strikethrough(value: Boolean = true): Unit = run { component.isStrikethrough = value }

    /**
     * Changes is the component underlined
     *
     * @param value the state if to make this component underlined or not
     */
    @JvmDefault
    public fun underlined(value: Boolean = true): Unit = run { component.isUnderlined = value }

    /**
     * Changes is the component obfuscated
     *
     * @param value the state if to make this component obfuscated or not
     */
    @JvmDefault
    public fun obfuscated(value: Boolean = true): Unit = run { component.isObfuscated = value }
}
