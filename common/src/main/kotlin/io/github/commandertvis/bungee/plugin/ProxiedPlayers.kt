package io.github.commandertvis.bungee.plugin

import net.md_5.bungee.api.connection.ProxiedPlayer

/**
 * The UUID string representation of this [ProxiedPlayer].
 */
public inline val ProxiedPlayer.uuidString: String
    get() = uniqueId.toString()
