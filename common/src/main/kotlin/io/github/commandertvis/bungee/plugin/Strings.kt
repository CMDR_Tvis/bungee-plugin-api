package io.github.commandertvis.bungee.plugin

import net.md_5.bungee.api.ChatColor
import java.util.*

/**
 * Replaces multiple placeholders in this [String]
 *
 * @param pairs pairs, where [Pair.first] is the key and [Pair.second] is the value
 */
public fun String.placeholders(vararg pairs: Pair<String, Any>): String {
    var string = this
    pairs.forEach { string = string.placeholder(it) }
    return string
}

/**
 * Constructs new [UUID] from this [String]
 *
 * @throws IllegalArgumentException when [UUID] from the certain string building is not possible
 * @return new [UUID] reference
 */
@Throws(IllegalArgumentException::class)
public fun String.toUuid(): UUID = UUID.fromString(this)!!

/**
 * Constructs new [UUID] from this [String]
 *
 * @return new [UUID] reference or null
 */
public fun String.toUuidOrNull(): UUID? = runCatching(String::toUuid).getOrNull()

/**
 * Formats messages by Bukkit color codes
 *
 * @return string where `&` color codes are replaced with \`u00A7` color codes
 */
public fun String.colorize(): String = ChatColor.translateAlternateColorCodes('&', this)

/**
 * Function to replace %placeholders% with values
 *
 * @param key key of the placeholder
 * @param value value to replace with
 * @return string where all the placeholders with the values
 */
public fun String.placeholder(key: String, value: Any): String = replace("%$key%", value.toString())

/**
 * Function to replace %placeholders% with values
 *
 * @param pair [Pair], where [Pair.first] is the key and [Pair.second] is the value
 * @return string where all the placeholders with the values
 */
public fun String.placeholder(pair: Pair<String, Any>): String = placeholder(pair.first, pair.second)
