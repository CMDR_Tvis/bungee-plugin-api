import org.jetbrains.dokka.ExternalDocumentationLinkImpl
import org.jetbrains.dokka.gradle.PackageOptions
import org.jetbrains.dokka.gradle.SourceRoot
import java.net.URL

plugins { `maven-publish`; id("org.jetbrains.dokka") version "0.9.18"; kotlin("jvm") version "1.3.50" }

allprojects {
    apply(plugin = "kotlin")
    description = "Plugin API for BungeeCord"
    group = "io.github.commandertvis.bungee.plugin"
    version = "1.0.0"

    repositories {
        jcenter()
        maven("https://oss.sonatype.org/content/groups/public/")
    }

    dependencies {
        compileOnly("net.md-5:bungeecord-api:1.13-SNAPSHOT")
        implementation("com.fasterxml.jackson.core:jackson-core:2.9.9")
        implementation("com.fasterxml.jackson.module:jackson-module-kotlin:2.9.7") { exclude("org.jetbrains.kotlin") }
        implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core:1.3.0-RC")
        implementation(kotlin("stdlib-jdk8"))
        implementation(kotlin("reflect"))
    }

    tasks.compileKotlin {
        kotlinOptions {
            val kotlinVersion = "1.3"
            apiVersion = kotlinVersion
            freeCompilerArgs = listOf("-XXLanguage:+InlineClasses", "-Xjvm-default=enable")
            jvmTarget = "1.8"
            languageVersion = kotlinVersion
        }
    }
}

subprojects {
    if (name == "runtime")
        return@subprojects

    apply(plugin = "maven-publish")

    tasks {
        create<Jar>("sourcesJar") { from(sourceSets.main.get().allSource); archiveClassifier.set("sources") }
        publish { dependsOn(build) }

        publishing {
            publications {
                create<MavenPublication>("mavenJava") {
                    from(this@subprojects.components["java"])
                    artifact(this@tasks["sourcesJar"])

                    pom {
                        operator fun <T> Property<T>.invoke(value: T) = set(value)

                        operator fun <T> SetProperty<T>.invoke(vararg values: T) = set(values.toList())

                        packaging = "jar"
                        name(project.name)
                        description(project.description)
                        url("https://gitlab.com/tvis/${project.name}")
                        inceptionYear("2019")

                        licenses {
                            license {
                                comments("Open-source license")
                                distribution("repo")
                                name("MIT License")
                                url("https://gitlab.com/tvis/${project.name}/blob/master/LICENSE")
                            }
                        }

                        developers {
                            developer {
                                email("postovalovya@gmail.com")
                                id("CMDR_Tvis")
                                name("Commander Tvis")
                                roles("architect", "developer")
                                timezone("7")
                                url("https://gitlab.com/CMDR_Tvis")
                            }
                        }
                    }
                }

                repositories {
                    maven("https://gitlab.com/api/v4/projects/13653883/packages/maven") {
                        credentials(HttpHeaderCredentials::class) {
                            name = "Job-Token"
                            value = System.getenv("CI_JOB_TOKEN")
                        }

                        authentication { register("header", HttpHeaderAuthentication::class) }
                    }
                }
            }
        }
    }
}

tasks.dokka {
    externalDocumentationLinks = arrayOf(
        "https://ci.md-5.net/job/BungeeCord/ws/api/target/apidocs/",
        "https://ci.md-5.net/job/BungeeCord/ws/chat/target/apidocs/",
        "http://fasterxml.github.io/jackson-databind/javadoc/2.9/",
        "https://static.javadoc.io/com.google.code.gson/gson/2.8.5/"
    ).mapTo(mutableListOf()) { ExternalDocumentationLinkImpl(URL(it), URL("${it}package-list")) }

    impliedPlatforms = mutableListOf("JVM")
    includes = listOf(file("PACKAGES.md"))
    moduleName = "main"
    outputDirectory = "public/"
    perPackageOptions = mutableListOf(PackageOptions().apply { prefix = "kotlin" })

    sourceRoots = arrayOf(
        "command/src/main/kotlin",
        "common/src/main/kotlin/"
    ).mapTo(mutableListOf()) { SourceRoot().apply { path = it } }
}
