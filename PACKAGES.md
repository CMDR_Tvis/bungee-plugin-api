# Package io.github.commandertvis.bungee.plugin
General API and plugin types, top-level bindings of Bukkit functions and values
# Package io.github.commandertvis.bungee.plugin.chatcomponents
Spigot chat components API extensions
# Package io.github.commandertvis.bungee.plugin.command
Bukkit command utilities
# Package io.github.commandertvis.bungee.plugin.command.contexts
Command contexts parsing API
# Package io.github.commandertvis.bungee.plugin.command.dsl
Bukkit command building DSL classes and functions
# Package io.github.commandertvis.bungee.plugin.command.dsl.component
Contains GUI components for handling typical GUI elements
# Package io.github.commandertvis.bungee.plugin.command.dsl.help
Help building DSL classes of command API
# Package io.github.commandertvis.bungee.plugin.json
Bukkit API extension to configure plugins and store information using JSON instead of YAML
# Package io.github.commandertvis.bungee.plugin.json.adapters
Builtin JSON adapters to use in JSON configurations
